#include <cppdb/frontend.h>
#include <cppcms/application.h>
#include <cppcms/applications_pool.h>
#include <cppcms/service.h>
#include <cppcms/http_response.h>
#include <cppcms/url_dispatcher.h>
#include <iostream>
#include <sstream>
#include <cstdlib>

class hell_odbc_service : public cppcms::application {
public:
  hell_odbc_service(cppcms::service &srv) :
    cppcms::application(srv) {
      // configure http endpoints
      dispatcher().assign("/sql", &hell_odbc_service::jsonification, this);
      dispatcher().assign("/vcap", &hell_odbc_service::print_env, this);
      dispatcher().assign("/number/(\\d+)",&hell_odbc_service::number, this, 1);
      dispatcher().assign("/smile", &hell_odbc_service::smile, this);
      dispatcher().assign("/", &hell_odbc_service::welcome, this);
    }

    void number(std::string num) {
      int no = atoi(num.c_str());
      response().out() << "The number is " << no << "\n";
    }

    void smile() {
      response().out() << ":-)";
    }

    void welcome() {
      response().out() <<
        "<h1> Wellcome To Page with links </h1>\n"
        "<a href=\"/number/1\">1</a><br>\n"
        "<a href=\"/number/15\">15</a><br>\n"
        "<a href=\"/smile\">:-)</a><br>\n";
    }

    void jsonification() {
      try {
        // dsn is the representation of ODBC DSN
        // we will construct it from the values available in app's env
        // all of the values, except 'driver' is available after
        // cf bs <app name> <existing hana hdi service>
        std::string dsn ("odbc:");

        cppcms::json::value vcap_object, hana_credentials;
        // load JSON-styled env and all the values from VCAP_SERVICES
        // see cf docs for more details
        // to see all app-wide env execute
        // cf env <app name>
        std::istringstream vcap(std::getenv("VCAP_SERVICES"));
        vcap_object.load(vcap, true);
        hana_credentials = vcap_object["hanatrial"][0]["credentials"];
        // configure driver
        // as an exception driver name is provided by
        // ups, i.e. user-provided-service
        // cf cups <service name> -p driver
        // cf bs <app name> <service name>
        dsn += "Driver=";
        dsn += vcap_object["user-provided"][0]["credentials"]["driver"].str();
        dsn += ";";

        // server name consists of
        dsn += "ServerNode=";
        // host name
        dsn += hana_credentials["host"].str();
        dsn += ":";
        // host port
        dsn += hana_credentials["port"].str();
        dsn += ";";

        // user name
        dsn += "UID=";
        dsn += hana_credentials["user"].str();
        dsn += ";";

        // password
        dsn += "PWD=";
        dsn += hana_credentials["password"].str();

        // as an example, our ODBC DSN should look like
        // "odbc:Driver=<driver name>;ServerNode=<host>:<port>;UID=<user name>;PWD=<password>"

        // sample of sql execution via cppdb using previously constructed ODBC DSN
        cppdb::session sql(dsn);
        cppdb::result res = sql << "select 'Ro Ro', 'Rossi', 'Ronni' from dummy";


        cppcms::json::value tmp_object;
        std::string tmp_name = "Mo Mo";
        std::string tmp_kids_names1 = "Yossi";
        std::string tmp_kids_names2 = "Yonni";

        // if there is an output from db then tmp values will be substituted
        while(res.next()) {
          res >> tmp_name >> tmp_kids_names1 >> tmp_kids_names2;
        }

        tmp_object["name"]=tmp_name;
        tmp_object["salary"]=1000.0;
        tmp_object["kids_names"][0]=tmp_kids_names1;
        tmp_object["kids_names"][1]=tmp_kids_names2;
        tmp_object["data"]["weight"]=85;
        tmp_object["data"]["height"]=1.80;

        // send JSON obj back to client
        // or simply response().out() << tmp_object;
        tmp_object.save(response().out(), cppcms::json::readable);
      } catch (std::exception const &e) {
        std::cerr << "ERROR: " << e.what() << std::endl;
        return;
      }
    }

    void print_env() {
        cppcms::json::value vcap_test_object;
        std::istringstream vcap_is(std::getenv("VCAP_SERVICES"));
        vcap_test_object.load(vcap_is, true);
        vcap_test_object.save(response().out(), cppcms::json::readable);
    }
};

int main(int argc, char ** argv) {
  try {
    cppcms::service srv(argc, argv);
    srv.applications_pool().mount(cppcms::applications_factory<hell_odbc_service>());
    srv.run();
  } catch(std::exception const &e) {
    std::cerr << e.what() << std::endl;
  }
}
